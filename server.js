require('dotenv').config()

const express = require("express"),
  path = require("path");

const app = express();

app.use("/", express.static(path.join(__dirname, "src")));

app.listen(process.env.PORT || 3000, () =>
  console.log(`Transport app listening on port ${process.env.PORT || 3000}!`)
);
