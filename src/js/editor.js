let Vue = window.Vue;

Vue.component("star-system", {
  props: ["node"],
  template: `<div>
  ID:{{node.id}} Group:{{node.group}} Store:{{node.store}} Type:{{node.type}}
  </div>
  `
});

let editor = new Vue({
  el: "#editor",
  data: () => {
    return {
      graph: {
        nodes: [
          {
            id: "earth",
            group: 1,
            store: 50,
            type: "hub"
          }
        ],
        links: []
      }
    };
  },
  methods: {
    btnClicked() {
      this.graph.nodes.push({ id: "Cool" });
    }
  }
});
