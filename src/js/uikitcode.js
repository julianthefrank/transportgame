Vue.component("HomePlanet", {
  props: {
    beamWH: { type: Number, required: true },
    shipWH: { type: Number, required: true }
  },
  template: `
  <div class="uk-card uk-card-body uk-card-default uk-width-auto uk-text-center uk-padding-small">
    <div class="uk-card-title">Home Planet</div>
      <span>Beam WareHouse : <span class="uk-badge">{{beamWH}}</span></span>
      <span>Ship WareHouse : <span class="uk-badge">{{shipWH}}</span></span>
  </div>`
});

Vue.component("ProducerPlanet", {
  props: {
    beamWH: { type: Number, required: true },
    shipWH: { type: Number, required: true }
  },
  template: `
  <div class="uk-card uk-card-body uk-card-default uk-width-auto uk-padding-small uk-text-center
  uk-margin-small-top uk-margin-small-bottom uk-margin-small-left uk-margin-small-right">
    <div class="uk-card-title">Producer Planet</div>
    <div>
      <div>Beam WareHouse : <span class="uk-badge">{{beamWH}}</span></div>
      <div>Ship WareHouse : <span class="uk-badge">{{shipWH}}</span></div>
    </div>
  </div>`
});

var mainApp = new Vue({
  el: `#mainApp`,
  template: `
  <div class="uk-container  uk-container-xsmall uk-padding-small">
    <HomePlanet :beamWH="10" :shipWH="100"></HomePlanet>
    <div class="uk-flex uk-flex-wrap" >
      <ProducerPlanet v-for="x in n" :beamWH="x" :shipWH="x*2"></HomePlanet>
    </div>
    
  </div>
        `,
  data: () => {
    return {
      a: "a",
      b: "b",
      n: 2
    };
  }
});
