class ProducerPlanet {
  constructor(params) {
    //params based inits
    params = params || {};
    this.star = params.star || "sun"; //Star System in which this Planet is located
    this.planet = params.planet || "Anonymous";
    this.storeCapacity = params.storeCapacity || { cages: 20 };
    this.transportCapacity = params.transportCapacity || {
      cages: { ship: 10, beam: 1 }
    }; //Should be a json with separate values for ship & beam
    this.production = () =>
      Math.round(Math.random() * (params.maxProduction || 20)); //Assume Production equal to total transport capacity
    this.Cost = params.cost || {
      storage: { cage: 1 }, //Cost of Storage of each Cage
      transport2starhub: { ship: 1, beam: 11 }, //Cost to transport to local Star Hub (Not used in scenario 1)
      transport2earth: { ship: 11, beam: 111 } //Cost to transport to Earth
    };
    //Processed Vars
    this.storageCost = 0;
    this.Stock = { cages: 0 };
    this.transportCost = {
      starhub: { beam: 0, ship: 0 },
      earth: { beam: 0, ship: 0 }
    };
    this.export = { beam: 0, ship: 0 }; //Capacity in Cages
    this.Wastage = { cages: 0 };
  }

  tick() {
    console.log(`Ticking for Planet:${this.planet}`);
    //Pay for storage so far
    this.storageCost += this.Stock.cages * this.Cost.storage.cage;
    //Lets do some production
    this.Stock.cages += this.production();
    //console.log("stock", this.Stock, this.transportCapacity.cages.beam);
    //Lets transport as much as possible
    this.export.beam =
      this.transportCapacity.cages.beam < this.Stock.cages
        ? this.transportCapacity.cages.beam
        : this.Stock.cages;
    this.Stock.cages -= this.export.beam;
    //console.log("stock after beam", this.Stock);
    this.export.ship =
      this.transportCapacity.cages.ship < this.Stock.cages
        ? this.transportCapacity.cages.ship
        : this.Stock.cages;
    this.Stock.cages -= this.export.ship;
    if (this.Stock.cages > this.storeCapacity.cages) {
      this.Wastage.cages = this.Stock.cages - this.storeCapacity.cages;
      this.Stock.cages = this.storeCapacity.cages;
    }
    //console.log("stock after ship", this.Stock);
    this.transportCost.starhub.beam +=
      this.export.beam * this.Cost.transport2starhub.beam;
    this.transportCost.starhub.ship +=
      this.export.ship * this.Cost.transport2starhub.ship;

    this.transportCost.earth.beam +=
      this.export.beam * this.Cost.transport2earth.beam;
    this.transportCost.earth.ship +=
      this.export.ship * this.Cost.transport2earth.ship;

    let res = {
      star: this.star,
      planet: this.planet,
      storageCost: this.storageCost,
      stock: this.Stock,
      export: this.export,
      transportCost: this.transportCost,
      wastage: this.Wastage
    };
    //console.log(JSON.stringify(res));
    return res;
  }
}

class StarHub {
  constructor(params) {
    //params based inits
    params = params || {};
    this.star = params.star || "sun"; //Star System in which this Planet is located
    this.storeCapacity = params.storeCapacity || { cages: 10, containers: 2 }; //Should be a json with separate values for ship & beam
    this.transportCapacity = params.transportCapacity || {
      cages: { ship: 2, beam: 2 },
      containers: { ship: 2, beam: 2 }
    }; //Should be a json with separate values for ship & beam
    this.processSpeed = params.processSpeed || 4; //How many Cages can be processed
    this.processRatio = params.processRatio || 0.1; //If 1 Food container is made by 10 cages then value is 1/10=0.1
    this.Cost = params.cost || {
      storage: { cage: 1, container: 1 }, //Storage Costs
      transport2earth: { ship: 1, beam: 1 } //Cost to Ship to Earth
    };
    //Connections
    this.planets = []; //connected planets

    //Processed Vars
    this.storageCost = 0;
    this.Stock = { cages: 0, containers: 0 };
    this.transportCost = { earth: { beam: 0, ship: 0 } };
    this.export = { beam: 0, ship: 0 }; //Capacity in Containers
    this.Wastage = { cages: 0, containers: 0 };
  }

  tick() {
    console.log(`Ticking for Hub:${this.star} Planets:${this.planets.length}`);
    let incoming = this.planets.map(planet => planet.tick());
    //console.log(JSON.stringify(incoming, "", " "));

    //Pay for storage so far
    this.storageCost +=
      this.Stock.cages * this.Cost.storage.cage +
      this.Stock.containers * this.Cost.storage.container;
    //Lets do some import
    this.Stock.cages = incoming.reduce((prev, curr) => {
      //console.log(prev, JSON.stringify(curr.export));
      return prev + curr.export.beam + curr.export.ship;
    }, this.Stock.cages);
    //console.log(`Stock after Import ${JSON.stringify(this.Stock)}`);

    //Lets now Process the Import
    if (this.processSpeed > this.Stock.cages) {
      this.Stock.containers += this.Stock.cages * this.processRatio;
      this.Stock.cages = 0;
    } else {
      this.Stock.containers += this.processSpeed * this.processRatio;
      this.Stock.cages -= this.processSpeed;
      //Check Wastage in Cages
      if (this.Stock.cages > this.storeCapacity.cages) {
        this.Wastage.cages += this.Stock.cages - this.storeCapacity.cages;
        this.Stock.cages = this.storeCapacity.cages;
      }
    }
    /*console.log(
        `Stock after Processing ${JSON.stringify(
          this.Stock
        )} with Wastage of ${JSON.stringify(this.Wastage)}`
      );*/
    //Lets transport as much as possible
    this.export.beam =
      this.transportCapacity.containers.beam < this.Stock.containers
        ? this.transportCapacity.containers.beam
        : this.Stock.containers;
    this.Stock.containers -= this.export.beam;
    //console.log("stock after beam", this.Stock);
    this.export.ship =
      this.transportCapacity.containers.ship < this.Stock.containers
        ? this.transportCapacity.containers.ship
        : this.Stock.containers;
    this.Stock.containers -= this.export.ship;
    //Check for container wastage
    if (this.Stock.containers > this.storeCapacity.containers) {
      this.Wastage.containers =
        this.Stock.containers - this.storeCapacity.containers;
      this.Stock.containers = this.storeCapacity.containers;
    }
    //console.log("stock after ship", this.Stock);
    //Lets work out the transport cost to earth
    this.transportCost.earth.beam +=
      this.export.beam * this.Cost.transport2earth.beam;
    this.transportCost.earth.ship +=
      this.export.ship * this.Cost.transport2earth.ship;

    let res = {
      star: this.star,
      planet: this.planet,
      storageCost: this.storageCost,
      stock: this.Stock,
      export: this.export,
      transportCost: this.transportCost,
      wastage: this.Wastage
    };
    console.log(JSON.stringify(res, "", " "));
    return res;
  }
}

class Earth {
  constructor(params) {
    //param based inits
    params = params || {};
    this.star = "sun"; //Star System in which this Planet is located
    this.scenario = params.scenario || 1;
    this.storeCapacity = params.storeCapacity || {
      cages: { ship: 2, beam: 2 },
      containers: { ship: 2, beam: 2 } //Containers carry food
    }; //Should be a json with separate values for ship & beam
    this.processSpeed = params.processSpeed || 4; //How many Cages can be processed
    this.processRatio = params.processRatio || 0.1; //If 1 Food container is made by 10 cages then value is 1/10=0.1
    this.Cost = params.cost || { storage: { cage: 1, container: 1 } };
    //Init connections
    this.hubs = []; //Holds the hubs connected to earth
    this.planets = []; //Holds the Planets connected to earth
  }

  tick() {
    console.log(
      `Ticking for Scenario:${this.scenario} Star:${this.star} Hubs:${
        this.hubs.length
      } Planets:${this.planets.length}`
    );
    this.planets.forEach(planet => planet.tick()); //Scenario 1

    this.hubs.forEach(hub => hub.tick()); //Scenario 2
  }
}

let starConfig = {
  earth: { star1: ["planet1", "planet2"], star2: ["planet3"] }
};

function genUniverse(data) {
  let scn1earth = new Earth({ scenario: 1 }),
    scn2earth = new Earth({ scenario: 2 });
  for (const star in data.earth) {
    if (data.earth.hasOwnProperty(star)) {
      const planets = data.earth[star];
      let starHub = new StarHub({ star: star });
      scn2earth.hubs.push(starHub);
      planets.forEach(planet => {
        //Process for Scenario 1
        scn1earth.planets.push(
          new ProducerPlanet({ star: star, planet: planet })
        );
        //Process for Scenario 2
        starHub.planets.push(
          new ProducerPlanet({ star: star, planet: planet })
        );
      });
    }
  }
  return { scenario1: scn1earth, scenario2: scn2earth };
}

let { scenario1, scenario2 } = genUniverse(starConfig);

let iterations = 2;

for (let i = 0; i < iterations; i++) {
  scenario1.tick();
  scenario2.tick();
}

let Node = Vue.component("Node", {
  template: `<div>
  name:{{name}}
  game:{{game}}
  </div>`,
  props: ["name"],
  data: () => {
    return {
      game: "game"
    };
  }
});

let mainApp = new Vue({
  el: "#mainApp",
  components: { Node },
  data: () => {
    return {
      name: "JF.."
    };
  }
});
