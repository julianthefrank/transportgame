let graph = {
  nodes: [
    {
      id: "earth",
      group: 1,
      store: 50,
      type: "hub"
    },
    {
      id: "star1",
      group: 2,
      store: 40,
      type: "hub"
    },
    {
      id: "planet1",
      group: 2,
      store: 10,
      type: "producer"
    },
    {
      id: "star2",
      group: 3,
      store: 60,
      type: "hub"
    },
    {
      id: "planet2",
      group: 3,
      store: 20,
      type: "producer"
    },
    {
      id: "planet3",
      group: 3,
      store: 30,
      type: "producer"
    }
  ],
  links: [
    {
      source: "star1",
      target: "earth",
      dist: 100
    },
    {
      source: "planet1",
      target: "star1",
      dist: 10
    },
    {
      source: "star2",
      target: "earth",
      dist: 200
    },
    {
      source: "planet2",
      target: "star2",
      dist: 20
    },
    {
      source: "planet3",
      target: "star2",
      dist: 30
    }
  ]
};

//set up the simulation
var svg = d3.select("#d3svg"),
  width = +svg.attr("width"),
  height = +svg.attr("height");

function drawGraph(graph) {
  document.getElementById("d3svg").innerHTML = "";
  var simulation = d3.forceSimulation().nodes(graph.nodes);
  //nodes only for now
  simulation
    .force(
      "charge",
      d3
        .forceManyBody()
        .distanceMax(44)
        .distanceMin(10)
    )
    .force("center", d3.forceCenter(width / 2, height / 2))
    .force("collision", d3.forceCollide().radius(d => d.store / 1.5));
  //draw circles for the nodes
  var node = svg
    .append("g")
    .attr("class", "nodes")
    .selectAll("circle")
    .data(graph.nodes)
    .enter()
    .append("circle")
    .attr("r", d => d.store / 2)
    .attr("fill", d => (d.type == "hub" ? "Red" : "Blue"));
  //Time for the links //Create the link force //We need the id accessor to use named sources and targets
  var link_force = d3
    .forceLink(graph.links)
    .id(d => d.id)
    .distance(d => d.dist);
  //Add a links force to the simulation//Specify links  in d3.forceLink argument
  simulation.force("links", link_force);
  //draw lines for the links
  var link = svg
    .append("g")
    .attr("class", "links")
    .selectAll("line")
    .data(graph.links)
    .enter()
    .append("line")
    .attr("stroke-width", 2);

  var labelNode = svg
    .append("g")
    .attr("class", "labelNodes")
    .selectAll("text")
    .data(graph.nodes)
    .enter()
    .append("text")
    .style("fill", "white")
    .style("font-family", "Arial")
    .style("font-size", 12)
    .style("font-weight", "bold")
    .style("text-shadow", "1px 1px 2px black, 0 0 25px blue, 0 0 5px darkblue")
    .style("pointer-events", "none"); // to prevent mouseover/drag capture
  //add tick instructions:
  simulation.on("tick", tickActions);
  function tickActions() {
    //update link positions//simply tells one end of the line to follow one node around//and the other end of the line to follow the other node around
    link
      .attr("x1", d => d.source.x)
      .attr("y1", d => d.source.y)
      .attr("x2", d => d.target.x)
      .attr("y2", d => d.target.y);
    //update circle positions each tick of the simulation
    node.attr("cx", d => d.x).attr("cy", d => d.y);
    labelNode
      .attr("x", d => d.x)
      .attr("y", d => d.y)
      .text(d => `${d.id.toUpperCase()}-[store:${d.store}]`);
  }
}

drawGraph(graph);
